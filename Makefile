.PHONY: all deps clean release

all: compile

compile: deps
	./rebar -j8 compile

deps:
	./rebar -j8 get-deps

clean:
	./rebar -j8 clean

relclean:
	rm -rf rel/elogic2

generate: compile
	cd rel && .././rebar -j8 generate

run: generate
	./rel/elogic2/bin/elogic2 start

console: generate
	./rel/elogic2/bin/elogic2 console

foreground: generate
	./rel/elogic2/bin/elogic2 foreground

erl: compile
	erl -pa ebin/ -pa deps/*/ebin/ -s elogic2
