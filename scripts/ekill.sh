#! /usr/bin/env bash

if [ $# -ne 1 ]; then
	echo "./ekill.sh <project>"
	exit
fi

project=$1

ps aux | grep $project | grep -v grep | awk '{print $2}' | xargs sudo kill -9
