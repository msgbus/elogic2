%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2016, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 04. 五月 2016 下午4:11
%%%-------------------------------------------------------------------
-author("zhengyinyong").

%%--------------------------------------------------------------------
%% elogic2 meta
%%--------------------------------------------------------------------
-record(internal_package_args, {
    from_ip,
    from_port,
    broker_score,
    protocol_version,
    uid,
    client_id,
    appkey,
    appid,
    platform
}).

-record(elogic2_state, {
    internal_package_args = #internal_package_args{},
    route_table_ttl = 300
}).

%%--------------------------------------------------------------------
%% elogic2 route table
%%--------------------------------------------------------------------
-record(elogic2_route_table, {
    appid,
    flag,
    ip,
    port,
    tag,
    time
}).

-define(ONLINE, 1).
-define(OFFLINE, 0).

%%--------------------------------------------------------------------
%% elogic2 worker pool
%%--------------------------------------------------------------------
-define(ELOGIC2_WORKER_POOL, elogic2_worker_pool).

%%--------------------------------------------------------------------
%% rabbitmq proxy
%%--------------------------------------------------------------------
-define(FRONTKEYPREFIX, <<"msgbus_frontend_key_">>).

%%--------------------------------------------------------------------
%% couchbase key
%%--------------------------------------------------------------------
-define(MSG_QUEUE_PREFIX, <<"msgq_">>).

%%--------------------------------------------------------------------
%% publish to alias
%%--------------------------------------------------------------------
-define(SET_ALIAS, ",yali").
-define(GET_ALIAS, ",yaliget").
-define(PUBLISH_TO_ALIAS, ",yta").

%%--------------------------------------------------------------------
%% http client
%%--------------------------------------------------------------------
-define(HTTP_TIMEOUT, 10000). %% timeout values are in milliseconds.

%%--------------------------------------------------------------------
%% topicfs
%%--------------------------------------------------------------------
-define(TOPICFS_ACTION_SUBSCRIBE,   "add").
-define(TOPICFS_ACTION_UNSUBSCRIBE, "rem").
-define(TOPICFS_ACTION_CONNECT,     "online").
-define(TOPICFS_ACTION_DISCONNECT,  "offline").
-define(TOPICFS_ACTION_SET_ALIAS,   "set_alias").
-define(TOPICFS_ACTION_GET_ALIAS,   "get_alias").
