%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2016, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 21. 六月 2016 下午5:14
%%%-------------------------------------------------------------------
-module(elogic2_storage).
-author("zhengyinyong").

-include("elogic2.hrl").
-include_lib("epublish/include/epublish_args.hrl").

-compile({parse_transform, lager_transform}).

%% API
-export([get_route_table_item/1, decode_route_table_item/1]).

-export([remove_msgid_list/3]).

-export([unset_alias/1, cleanup_old_uid_alias/2, set_new_alias/2,
         get_uid_by_alias/2, get_alias_by_uid/2]).

%%%===================================================================
%%% route table storage operation
%%%===================================================================
get_route_table_item(Uid) ->
    case elogic2_redis:read_record(redis_route_table_pool, Uid, elogic2_route_table) of
        {ok, Item}  when Item#elogic2_route_table.flag =/= undefined ->
            #elogic2_route_table{flag = Flag, ip = Ip, port = Port, tag = Tag} = Item,
            {ok, {Flag, Ip, Port, Tag}};
        _ ->
            {error, key_enoent}
    end.

decode_route_table_item(RouteItem) ->
    case jiffy:decode(RouteItem) of
        {Props} ->
            Flag = elogic2_utils:get_key_value(<<"flag">>, Props),
            Ip = elogic2_utils:get_key_value(<<"ip">>, Props),
            Port = elogic2_utils:get_key_value(<<"port">>, Props),
            Tag = elogic2_utils:get_key_value(<<"tag">>, Props),
            {ok, {Flag, Ip, Port, Tag}};
        Else -> %% {error,{1,invalid_json}}
            Else
    end.

%%%===================================================================
%%% message queue storage operation
%%%===================================================================
remove_msgid_list(Uid, MessageId, _EpublishMeta) ->
    Key = iolist_to_binary([elogic2_utils:make_sure_binary(?MSG_QUEUE_PREFIX), elogic2_utils:make_sure_binary(Uid)]),
    case cbclient_storage:lremove(msg_queue, Key, 0, elogic2_utils:make_sure_integer(MessageId)) of
        ok ->
            ok;
        {error, Reason} ->
            {error, Reason}
    end.

%%%===================================================================
%%% alias storage operation
%%%===================================================================
unset_alias(Elogic2State) ->
    case remove_old_alias_to_uid(<<"">>, Elogic2State) of
        ok ->
            Uid = Elogic2State#elogic2_state.internal_package_args#internal_package_args.uid,
            cbclient_storage:rm(alias, Uid);
        Else ->
            Else
    end.

cleanup_old_uid_alias(Alias, Elogic2State) ->
    case remove_old_uid_to_alias(Alias, Elogic2State) of
        ok ->
            remove_old_alias_to_uid(Alias, Elogic2State);
        Else ->
            Else
    end.

set_new_alias(Alias, Elogic2State) ->
    Uid = Elogic2State#elogic2_state.internal_package_args#internal_package_args.uid,
    Result1 = cbclient_storage:set(alias,
                 uid_to_alias_key(Uid, Elogic2State), 0,
                 elogic2_utils:make_sure_string(Alias), str),
    Result2 = cbclient_storage:set(alias,
                 alias_to_uid_key(Alias, Elogic2State), 0,
                 elogic2_utils:make_sure_integer(Uid)),
    case {Result1, Result2} of
        {ok, ok} ->
            ok;
        Else ->
            Else
    end.

remove_old_uid_to_alias(Alias, Elogic2State) ->
    Uid = elogic2_utils:make_sure_binary(Elogic2State#elogic2_state.internal_package_args#internal_package_args.uid),
    case get_uid_by_alias(Alias, Elogic2State) of
        {ok, Uid} -> %% same UID
            ok;
        {ok, OldUid} ->
            cbclient_storage:rm(alias, OldUid);
        {error, key_enoent} -> %% key isn't exist
            ok;
        Error ->
            Error
    end.

remove_old_alias_to_uid(Alias, Elogic2State) ->
    Uid = Elogic2State#elogic2_state.internal_package_args#internal_package_args.uid,
    case get_alias_by_uid(Uid, Elogic2State) of
        {ok, Alias} ->
            ok;
        {ok, OldAlias} ->
            cbclient_storage:rm(alias, alias_to_uid_key(OldAlias, Elogic2State));
        {error, key_enoent} ->
            ok;
        Error ->
            Error
    end.

get_uid_by_alias(Alias, Elogic2State) ->
    Key = alias_to_uid_key(Alias, Elogic2State),
    case cbclient_storage:get(alias, Key, raw_binary) of
        {ok, Uid} ->
            {ok, Uid};
        Error ->
            Error
    end.

get_alias_by_uid(Uid, Elogic2State) ->
    Key = uid_to_alias_key(Uid, Elogic2State),
    case cbclient_storage:get(alias, Key, str) of
        {ok, Alias} ->
            {ok, Alias};
        Error ->
            Error
    end.

alias_to_uid_key(Alias, Elogic2State) ->
    Appkey = Elogic2State#elogic2_state.internal_package_args#internal_package_args.appkey,
    iolist_to_binary([elogic2_utils:make_sure_binary(Appkey), <<"-">>, elogic2_utils:make_sure_binary(Alias)]).

uid_to_alias_key(Uid, _Elogic2State) ->
    iolist_to_binary([elogic2_utils:make_sure_binary(Uid)]).
