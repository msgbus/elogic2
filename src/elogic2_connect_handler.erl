%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2016, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 12. 五月 2016 上午11:39
%%%-------------------------------------------------------------------
-module(elogic2_connect_handler).
-author("zhengyinyong").

-include("elogic2.hrl").
-include_lib("epublish/include/epublish_args.hrl").
-include_lib("msgbus_common_utils/include/internal_package_pb.hrl").

-compile({parse_transform, lager_transform}).

%% API
-export([handle_connect/2, handle_disconnect/2]).

handle_connect(_MqttFrame, Elogic2State) ->
    check_offline_message(Elogic2State),
    update_route_table(online, Elogic2State),
    update_broker_score(connect, empty, Elogic2State).

handle_disconnect(_MqttFrame, Elogic2State) ->
    update_route_table(offline, Elogic2State),
    update_broker_score(disconnect, empty, Elogic2State).

%%%===================================================================
%%% Internal functions
%%%===================================================================
update_broker_score(Type, Topic, Elogic2State) ->
    Result = elogic2_topicfs:request_topicfs(elogic2_topicfs:generate_request_to_topicfs(Type, Topic, Elogic2State)),
    Result.

update_route_table(Flag, Elogic2State) ->
    Appid = Elogic2State#elogic2_state.internal_package_args#internal_package_args.appid,
    FromIp = Elogic2State#elogic2_state.internal_package_args#internal_package_args.from_ip,
    FromPort = Elogic2State#elogic2_state.internal_package_args#internal_package_args.from_port,
    FromTag = Elogic2State#elogic2_state.internal_package_args#internal_package_args.broker_score,
    Uid = Elogic2State#elogic2_state.internal_package_args#internal_package_args.uid,
    TTL = Elogic2State#elogic2_state.route_table_ttl,
    FlagNum = case Flag of
                  online -> ?ONLINE;
                  offline -> ?OFFLINE
              end,
    ok = elogic2_redis:write_record(redis_route_table_pool, Uid,
        #elogic2_route_table{appid = Appid, flag = FlagNum, ip = FromIp, port = FromPort, tag = FromTag, time = elogic2_utils:get_millisec()},
        TTL).

check_offline_message(Elogic2State) ->
    PublishArgs = elogic2_utils:generate_publish_args(publish_offline_message, empty, Elogic2State),
    epublish:publish(PublishArgs).
