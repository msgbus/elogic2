%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2016, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 06. 五月 2016 上午10:34
%%%-------------------------------------------------------------------
-module(elogic2_utils).
-author("zhengyinyong").

-include("elogic2.hrl").
-include_lib("yunba_mqtt_serialiser/include/yunba_mqtt_serialiser.hrl").
-include_lib("epublish/include/epublish_args.hrl").

-compile({parse_transform, lager_transform}).

%% API
-export([make_sure_binary/1, make_sure_string/1, make_sure_integer/1, make_sure_atom/1]).
-export([http_get/1, http_get/2, http_post/2, http_post/3]).
-export([verify_appkey/1, get_env/3, clientid_to_appid/1, split_list_into_chunk/2, get_millisec/0,
    get_key_value/2, get_key_value/3, generate_message_id/0, generate_publish_args/3, generate_publish_mqtt_frame/4]).

verify_appkey(_Elogic2State) ->
    ok.

make_sure_binary(Data) ->
    if
        is_list(Data) ->
            list_to_binary(Data);
        is_integer(Data) ->
            integer_to_binary(Data);
        is_atom(Data) ->
            atom_to_binary(Data, latin1);
        true ->
            Data
    end.

make_sure_string(Data) ->
    if
        is_binary(Data) ->
            binary_to_list(Data);
        is_integer(Data) ->
            integer_to_list(Data);
        is_atom(Data) ->
            atom_to_list(Data);
    true ->
        Data
    end.

make_sure_integer(Data) ->
    if
        is_binary(Data) ->
            binary_to_integer(Data);
        is_list(Data) ->
            list_to_integer(Data);
        is_integer(Data) ->
            Data;
        true ->
            Data
    end.

make_sure_atom(Data) ->
    if
        is_binary(Data) ->
            binary_to_atom(Data, utf8);
        is_list(Data) ->
            list_to_atom(Data);
        is_atom(Data) ->
            Data;
        true ->
            Data
    end.

get_env(Applicaiton, Module, Arg) ->
    case application:get_env(Applicaiton, Module) of
        {ok, ArgsLists} ->
            case proplists:get_value(Arg, ArgsLists) of
                undefined ->
                    undefined;
                Value ->
                    Value
            end
    end.

clientid_to_appid(ClientId) ->
    case binary:split(elogic2_utils:make_sure_binary(ClientId), <<"-">>, []) of
        [AppId, _AppAccumulationId] ->
            {ok, integer_to_binary(binary_to_integer(AppId))};
        _ ->
            {error, ClientId}
    end.

http_get(URL) ->
    http_get(URL, ?HTTP_TIMEOUT).

http_get(URL, Timeout) ->
    case http_get_request(URL, Timeout) of
        {error, connection_closing} ->
            lager:error("<http get failed by server closing connection, retrying ...>[url:~p]~n", [URL]),
            http_get_request(URL, Timeout);
        Else ->
            Else
    end.

http_post(URL, Content) ->
    http_post(URL, Content, ?HTTP_TIMEOUT).

http_post(URL, Content, Timeout) ->
    case http_post_request(URL, Content, Timeout) of
        {error, connection_closing} ->
            lager:error("<http post failed by server closing connection, retry ...>[url:~p][content:~p]~n", [URL, Content]),
            http_post_request(URL, Content, Timeout);
        Else ->
            Else
    end.

split_list_into_chunk(List, Len) ->
    split_list_into_chunk(lists:reverse(List),[],0,Len).
split_list_into_chunk([], Acc, _, _) -> lists:reverse(Acc);
split_list_into_chunk([H|T], Acc, Pos, Max) when Pos == Max ->
    split_list_into_chunk(T, [[H] | Acc], 1, Max);
split_list_into_chunk([H|T],[HAcc|TAcc],Pos,Max) ->
    split_list_into_chunk(T, [[H|HAcc] | TAcc], Pos+1, Max);
split_list_into_chunk([H|T], [], Pos, Max) ->
    split_list_into_chunk(T, [[H]], Pos+1, Max).

get_millisec() ->
    {Mega, Sec, Micro} = os:timestamp(),
    (Mega*1000000 + Sec)*1000 + round(Micro/1000).

get_key_value(Key, List) ->
    case lists:keyfind(Key, 1, List) of
        false ->
            undefined;
        {_K, Value} ->
            Value
    end.

get_key_value(Key, List, Default) ->
    case lists:keyfind(Key, 1, List) of
        false ->
            Default;
        {_K, Value} ->
            Value
    end.

generate_message_id() ->
    MessageIdBin = snowflake:new(),
    <<MessageId:64/integer>> = MessageIdBin,
    MessageId.

generate_publish_args(Type, empty, Elogic2State) ->
    #epublish_args {
        type = Type,
        uid = Elogic2State#elogic2_state.internal_package_args#internal_package_args.uid,
        appkey = Elogic2State#elogic2_state.internal_package_args#internal_package_args.appkey,
        appid = Elogic2State#elogic2_state.internal_package_args#internal_package_args.appid,
        client_id = Elogic2State#elogic2_state.internal_package_args#internal_package_args.client_id,
        platform = Elogic2State#elogic2_state.internal_package_args#internal_package_args.platform,
        protocol_version = Elogic2State#elogic2_state.internal_package_args#internal_package_args.protocol_version
    };
generate_publish_args(Type, MqttFrame, Elogic2State) ->
   #epublish_args {
       type = Type,
       broker_score = Elogic2State#elogic2_state.internal_package_args#internal_package_args.broker_score,
       uid = Elogic2State#elogic2_state.internal_package_args#internal_package_args.uid,
       appkey = Elogic2State#elogic2_state.internal_package_args#internal_package_args.appkey,
       appid = Elogic2State#elogic2_state.internal_package_args#internal_package_args.appid,
       client_id = Elogic2State#elogic2_state.internal_package_args#internal_package_args.client_id,
       platform = Elogic2State#elogic2_state.internal_package_args#internal_package_args.platform,
       protocol_version = Elogic2State#elogic2_state.internal_package_args#internal_package_args.protocol_version,
       message_id = MqttFrame#mqtt_frame.variable#mqtt_frame_publish.message_id,
       qos = MqttFrame#mqtt_frame.fixed#mqtt_frame_fixed.qos,
       payload = MqttFrame#mqtt_frame.payload,
       topic = MqttFrame#mqtt_frame.variable#mqtt_frame_publish.topic_name
   }.

generate_publish_mqtt_frame(MessageId, QoS, Payload, Topic) ->
    #mqtt_frame {
        fixed = #mqtt_frame_fixed {
            qos = QoS
        },
        variable = #mqtt_frame_publish {
            message_id = MessageId,
            topic_name = Topic
        },
        payload = Payload
    }.

%%%===================================================================
%%% Internal functions
%%%===================================================================
http_get_request(URL, Timeout) ->
    try ibrowse:send_req(URL,[{"accept", "application/json"}],get, [], [], Timeout) of
        {ok, ReturnCode, _Headers, Body} ->
            case ReturnCode of
                "200" ->
                    lager:debug("http get client ~p succeed ~p", [URL, Body]),
                    {ok, Body};
                _Other ->
                    lager:error("http get client ~p not return 200 ~p~n", [URL, _Other]),
                    {error, Body}
            end;
        {error, connection_closing} ->
            {error, connection_closing};
        {error, REASON} ->
            lager:error("http get client ~p failed ~p~n", [URL, REASON]),
            {error, <<"http failed">>}
    catch
        Type:Error ->
            lager:error("http get client ~p failed ~p:~p~n", [URL, Type, Error]),
            {error, <<"http failed">>}
    end.

http_post_request(URL, Content, Timeout) ->
    try ibrowse:send_req(URL,[{"content-type", "application/json"}],post, Content, [], Timeout) of
        {ok, ReturnCode, _Headers, Body} ->
            case ReturnCode of
                "200" ->
                    lager:debug("http post client ~p succeed ~p", [URL, Body]),
                    {ok, Body};
                _Other ->
                    lager:error("http post client ~p not return 200 ~p ~p~n", [URL, Content, _Other]),
                    {error, Body}
            end;
        {error, connection_closing} ->
            {error, connection_closing};
        {error, REASON} ->
            lager:error("http post client ~p failed ~p ~p~n", [URL, Content, REASON]),
            {error, <<"http failed">>}
    catch
        Type:Error ->
            lager:error("http post client ~p failed ~p ~p:~p~n", [URL, Content, Type, Error]),
            {error, <<"http failed">>}
    end.
