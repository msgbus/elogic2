%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2016, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 22. 五月 2016 上午11:53
%%%-------------------------------------------------------------------
-module(elogic2_package_formatter).
-author("zhengyinyong").

-include("elogic2.hrl").
-include_lib("yunba_mqtt_serialiser/include/yunba_mqtt_serialiser.hrl").
-include_lib("msgbus_common_utils/include/internal_package_pb.hrl").

%% API
-export([form_internal_package_suback/2, form_internal_package_unsuback/2,
         form_internal_package_puback/2, form_internal_package_pubrec/2,
         form_internal_package_pubcomp/2]).

%%%===================================================================
%%% Internal Package Formatter
%%%===================================================================
form_internal_package_suback(MqttFrame, Elogic2State) ->
    FrontTag = Elogic2State#elogic2_state.internal_package_args#internal_package_args.broker_score,
    ProtocolVersion = Elogic2State#elogic2_state.internal_package_args#internal_package_args.protocol_version,
    Uid = Elogic2State#elogic2_state.internal_package_args#internal_package_args.uid,
    #internalpackage {
        from_ip = 0, from_port = 0,
        to_ip = 0, to_port = 0, client_id = "",
        protocol_version = ProtocolVersion,
        from_uid = elogic2_utils:make_sure_integer(Uid),
        from_tag = elogic2_utils:make_sure_binary(FrontTag),
        mqtt_package = form_mqtt_package_suback(MqttFrame, ProtocolVersion)
    }.

form_internal_package_unsuback(MqttFrame, Elogic2State) ->
    FrontTag = Elogic2State#elogic2_state.internal_package_args#internal_package_args.broker_score,
    ProtocolVersion = Elogic2State#elogic2_state.internal_package_args#internal_package_args.protocol_version,
    Uid = Elogic2State#elogic2_state.internal_package_args#internal_package_args.uid,
    #internalpackage {
        from_ip = 0, from_port = 0,
        to_ip = 0, to_port = 0, client_id = "",
        protocol_version = ProtocolVersion,
        from_uid = elogic2_utils:make_sure_integer(Uid),
        from_tag = elogic2_utils:make_sure_binary(FrontTag),
        mqtt_package = form_mqtt_package_unsuback(MqttFrame, ProtocolVersion)
    }.

form_internal_package_puback(MqttFrame, Elogic2State) ->
    form_internal_package_pub_type(?PUBACK, MqttFrame, Elogic2State).

form_internal_package_pubrec(MqttFrame, Elogic2State) ->
    form_internal_package_pub_type(?PUBREC, MqttFrame, Elogic2State).

form_internal_package_pubcomp(MqttFrame, Elogic2State) ->
    form_internal_package_pub_type(?PUBCOMP, MqttFrame, Elogic2State).

%%%===================================================================
%%% Internal functions
%%%===================================================================
form_mqtt_package_suback(MqttFrame, ProtocolVersion) ->
    TopicTable = MqttFrame#mqtt_frame.variable#mqtt_frame_subscribe.topic_table,
    MessageId = MqttFrame#mqtt_frame.variable#mqtt_frame_subscribe.message_id,
    QoSTable = [Topic#mqtt_topic.qos || Topic <- TopicTable],
    MqttPackage = #mqtt_frame {
        fixed = #mqtt_frame_fixed {
            type = ?SUBACK
        },
        variable = #mqtt_frame_suback {
            message_id = MessageId,
            qos_table = QoSTable
        }
    },
    MqttPackageBytes = yunba_mqtt_serialiser:serialise(MqttPackage, ProtocolVersion),
    MqttPackageBytes.

form_mqtt_package_unsuback(MqttFrame, ProtocolVersion) ->
    MessageId = MqttFrame#mqtt_frame.variable#mqtt_frame_subscribe.message_id,
    MqttPackage = #mqtt_frame{
        fixed = #mqtt_frame_fixed {
            type = ?UNSUBACK
        },
        variable = #mqtt_frame_unsuback {
            message_id = MessageId
        }
    },
    MqttPackageBytes = yunba_mqtt_serialiser:serialise(MqttPackage, ProtocolVersion),
    MqttPackageBytes.

form_internal_package_pub_type(Type, MqttFrame, Elogic2State) ->
    FrontTag = Elogic2State#elogic2_state.internal_package_args#internal_package_args.broker_score,
    ProtocolVersion = Elogic2State#elogic2_state.internal_package_args#internal_package_args.protocol_version,
    Uid = Elogic2State#elogic2_state.internal_package_args#internal_package_args.uid,
    MqttPackageBytes = form_mqtt_package_pub_type(Type, MqttFrame, ProtocolVersion),

    InternalPackage = #internalpackage {
        from_ip = 0, from_port = 0,
        to_ip = 0, to_port = 0,
        client_id = "",
        protocol_version = ProtocolVersion,
        from_uid = elogic2_utils:make_sure_integer(Uid),
        from_tag = elogic2_utils:make_sure_binary(FrontTag),
        mqtt_package = MqttPackageBytes
    },

    InternalPackage.

form_mqtt_package_pub_type(Type, MqttFrame, ProtocolVersion) ->
    MqttPackage = #mqtt_frame{
        fixed = #mqtt_frame_fixed {
            type = Type,
            dup = 0,
            qos = 0,
            retain = 0
        },
        variable = #mqtt_frame_publish {
            message_id = MqttFrame#mqtt_frame.variable#mqtt_frame_publish.message_id
        }
    },
    MqttPackageBytes = yunba_mqtt_serialiser:serialise(MqttPackage, ProtocolVersion),
    MqttPackageBytes.
