%%%-------------------------------------------------------------------
%%% @author zy
%%% @copyright (C) 2016, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 15. 三月 2016 3:34 PM
%%%-------------------------------------------------------------------
-module(elogic2_redis).
-author("zy").

%% API
-export([init/0, write_record/3, write_record/4, read_record/3, read_records/3, get/2, set/3, get_cache/3]).

-compile([{parse_transform, lager_transform}]).

-define(REDIS_QUERY_TIMEOUT, (5 * 1000)).

init() ->
    ok.

write_record(PoolName, Key, Record) ->
    set(PoolName, Key, jiffy:encode({Record:to_json()})).

write_record(PoolName, Key, Record, TTLSeconds) ->
    case catch redis_hapool:q(PoolName, [<<"SET">>, Key, jiffy:encode({Record:to_json()}), <<"EX">>, TTLSeconds], ?REDIS_QUERY_TIMEOUT) of
        {ok, _} ->
            ok;
        {'EXIT', {timeout, _}} ->
            timeout;
        {error, Error} ->
            lager:error("error: ~p ~p", [Record, Error]),
            query_error;
        Error ->
            lager:error("unexpected error: ~p", [Error]),
            error
    end.

read_record(PoolName, Key, Record) ->
    case get(PoolName, Key) of
        {ok, Data} ->
            case catch jiffy:decode(Data) of
                {error, _} ->
                    lager:error("invalid data: ~p", [Data]),
                    invalid_data;
                {Object} ->
                    case Record:from_json(Object) of
                        {ok, Record2} ->
                            {ok, Record2};
                        Error ->
                            lager:error("unexpected error: ~p", [Error]),
                            error
                    end;
                Error ->
                    lager:error("unexpected error: ~p", [Error]),
                    error
            end;
        Else ->
            Else
    end.

read_records(_PoolName, [], _Record) ->
    {ok, []};
read_records(PoolName, Keys, Record) when is_list(Keys) ->
    case catch redis_hapool:q(PoolName, [<<"MGET">> | Keys], ?REDIS_QUERY_TIMEOUT) of
        {ok, DataList} ->
            Objects = lists:map(
                fun(undefined) ->
                    not_found;
                    (Item) ->
                        case catch jiffy:decode(Item) of
                            {error, _} ->
                                lager:error("invalid data: ~p", [Item]),
                                invalid_data;
                            {Obj} ->
                                case Record:from_json(Obj) of
                                    {ok, Record2} ->
                                        Record2;
                                    Error2 ->
                                        lager:error("unexpected error: ~p", [Error2]),
                                        invalid_data
                                end;
                            Error ->
                                lager:error("unexpected error: ~p", [Error]),
                                invalid_data
                        end
                end, DataList),
            {ok, Objects};
        {'EXIT', {timeout, _}} ->
            timeout;
        Error ->
            lager:error("unexpected error: ~p", [Error]),
            error
    end.

get(PoolName, Key) ->
    case catch redis_hapool:q(PoolName, [<<"GET">>, Key], ?REDIS_QUERY_TIMEOUT) of
        {ok, undefined} ->
            not_found;
        {ok, Data} ->
            {ok, Data};
        {'EXIT', {timeout, _}} ->
            timeout;
        Error ->
            lager:error("unexpected error: ~p", [Error]),
            error
    end.

set(PoolName, Key, Value) ->
    case catch redis_hapool:q(PoolName, [<<"SET">>, Key, Value], ?REDIS_QUERY_TIMEOUT) of
        {ok, _} ->
            ok;
        {'EXIT', {timeout, _}} ->
            timeout;
        {error, Error} ->
            lager:error("error: ~p ~p", [Value, Error]),
            query_error;
        Error ->
            lager:error("unexpected error: ~p", [Error]),
            error
    end.

get_cache(PoolName, Key, PersistenceFun) ->
    case get(PoolName, Key) of
        {ok, Value} ->
            {ok, Value};
        _Else ->
            case PersistenceFun(Key) of
                {ok, Value} ->
                    {ok, Value};
                not_found ->
                    not_found;
                Error ->
                    {error, Error}
            end
    end.