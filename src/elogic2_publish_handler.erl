%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2016, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 06. 五月 2016 下午5:00
%%%-------------------------------------------------------------------
-module(elogic2_publish_handler).
-author("zhengyinyong").

-include("elogic2.hrl").
-include_lib("yunba_mqtt_serialiser/include/yunba_mqtt_serialiser.hrl").
-include_lib("msgbus_common_utils/include/internal_package_pb.hrl").

-compile({parse_transform, lager_transform}).

%% API
-export([send_to_front/2]).

-export([handle_publish/2, handle_pubrel/2, handle_pubrec/2, handle_pubcomp/2, handle_puback/2]).

send_to_front(FrontTag, InternalPackage) ->
    FrontTagBin = elogic2_utils:make_sure_binary(FrontTag),
    RoutingKey = << ?FRONTKEYPREFIX/binary, FrontTagBin/binary >>,
    BytesWithHeader = internal_package_pb:encode_internalpackage(InternalPackage),
    lager:debug("<send to front>[routing_key:~p]~n", [RoutingKey]),
    msgbus_amqp_proxy:send(RoutingKey, list_to_binary(BytesWithHeader)).

handle_publish(MqttFrame, Elogic2State) ->
    publish(MqttFrame, Elogic2State).

handle_pubrel(MqttFrame, Elogic2State) ->
    case epublish:publish(elogic2_utils:generate_publish_args(publish_by_msgid, MqttFrame, Elogic2State)) of
        ok ->
            send_pubcomp(MqttFrame, Elogic2State);
        {error, Reason} ->
            lager:error("publish by message id failed ~p~n", [Reason])
    end.

handle_pubrec(MqttFrame, Elogic2State) ->
    send_pubrec(MqttFrame, Elogic2State).

handle_pubcomp(MqttFrame, Elogic2State) ->
    MessageId = MqttFrame#mqtt_frame.variable#mqtt_frame_publish.message_id,
    Uid = Elogic2State#elogic2_state.internal_package_args#internal_package_args.uid,
    %epublish:remove_msgid_from_queue(Uid, MessageId),
    {MessageId, Uid}.

handle_puback(MqttFrame, Elogic2State) ->
    MessageId = MqttFrame#mqtt_frame.variable#mqtt_frame_publish.message_id,
    Uid = Elogic2State#elogic2_state.internal_package_args#internal_package_args.uid,
    %epublish:remove_msgid_from_queue(Uid, MessageId),
    {MessageId, Uid}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
publish(MqttFrame, Elogic2State) ->
    Topic = MqttFrame#mqtt_frame.variable#mqtt_frame_publish.topic_name,
    case Topic of
        ?SET_ALIAS ->
            process_set_alias(MqttFrame, Elogic2State);
        ?GET_ALIAS ->
            process_get_alias(MqttFrame, Elogic2State);
        _ ->
            case lists:prefix(?PUBLISH_TO_ALIAS, Topic) of
                true ->
                    Alias = list_to_binary(string:substr(Topic, (string:len(",yta/") + 1))),
                    publish_to_alias(Alias, MqttFrame, Elogic2State);
                false ->
                    publish_to_topic(MqttFrame, Elogic2State)
            end
    end.

process_set_alias(MqttFrame, Elogic2State) ->
    Alias = MqttFrame#mqtt_frame.payload,
    case set_alias(Alias, Elogic2State) of
        ok ->
            send_puback(MqttFrame, Elogic2State);
        _Else ->
            ignore
    end.

process_get_alias(MqttFrame, Elogic2State) ->
    case get_alias(Elogic2State) of
        {ok, Alias} ->
            send_puback(MqttFrame, Elogic2State),
            MqttPublishFrame = MqttFrame#mqtt_frame{payload = Alias},
            epublish:publish(elogic2_utils:generate_publish_args(publish_to_one_uid, MqttPublishFrame, Elogic2State));
        _Else ->
            ignore
    end.

publish_to_alias(Alias, MqttFrame, Elogic2State) ->
    case elogic2_storage:get_uid_by_alias(Alias, Elogic2State) of
        {ok, Uid} ->
            case elogic2_storage:get_route_table_item(Uid) of
                {ok, {Flag, Ip, Port, Tag}} ->
                    case Flag of
                        ?ONLINE ->
                            InternalPackageArgs = Elogic2State#elogic2_state.internal_package_args#internal_package_args {
                                uid = Uid, broker_score = Tag, from_ip = Ip, from_port = Port
                            },
                            Elogic2State1 = Elogic2State#elogic2_state {internal_package_args = InternalPackageArgs},
                            epublish:publish(elogic2_utils:generate_publish_args(publish_to_one_uid, MqttFrame, Elogic2State1));
                        ?OFFLINE ->
                            ignore
                    end;
                _Else ->
                    ignore
            end;
        _Else ->
            ignore
    end.

publish_to_topic(MqttFrame, Elogic2State) ->
    QoS = MqttFrame#mqtt_frame.fixed#mqtt_frame_fixed.qos,
    Result = epublish:publish(elogic2_utils:generate_publish_args(publish_to_topic, MqttFrame, Elogic2State)),
    case Result of
        ok ->
            case QoS > 0 of
                true ->
                    send_puback(MqttFrame, Elogic2State);
                false ->
                    ignore
            end;

        {error, Reason} ->
            lager:error("publish to topic failed : ~p~n", [Reason]),
            ignore
    end.

send_puback(MqttFrame, Elogic2State) ->
    FrontTagBin = elogic2_utils:make_sure_binary(Elogic2State#elogic2_state.internal_package_args#internal_package_args.broker_score),
    InternalPackage = elogic2_package_formatter:form_internal_package_puback(MqttFrame, Elogic2State),
    send_to_front(FrontTagBin, InternalPackage).

send_pubrec(MqttFrame, Elogic2State) ->
    FrontTagBin = elogic2_utils:make_sure_binary(Elogic2State#elogic2_state.internal_package_args#internal_package_args.broker_score),
    InternalPackage = elogic2_package_formatter:form_internal_package_pubrec(MqttFrame, Elogic2State),
    send_to_front(FrontTagBin, InternalPackage).

send_pubcomp(MqttFrame, Elogic2State) ->
    FrontTagBin = elogic2_utils:make_sure_binary(Elogic2State#elogic2_state.internal_package_args#internal_package_args.broker_score),
    InternalPackage = elogic2_package_formatter:form_internal_package_pubcomp(MqttFrame, Elogic2State),
    send_to_front(FrontTagBin, InternalPackage).

set_alias(Alias, Elogic2State) ->
    case Alias of
        <<"">> ->
            elogic2_storage:unset_alias(Elogic2State);
        _ ->
            case elogic2_storage:cleanup_old_uid_alias(Alias, Elogic2State) of
                ok ->
                    elogic2_storage:set_new_alias(Alias, Elogic2State);
                Else ->
                    Else
            end
    end.

get_alias(Elogic2State) ->
    Uid = Elogic2State#elogic2_state.internal_package_args#internal_package_args.uid,
    case elogic2_storage:get_alias_by_uid(Uid, Elogic2State) of
        {ok, Alias} ->
            {ok, elogic2_utils:make_sure_binary(Alias)};
        {error, key_enoent} ->
            {ok, <<"">>};
        Error ->
            Error
    end.

