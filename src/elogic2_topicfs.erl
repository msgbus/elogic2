%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2016, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 11. 五月 2016 上午11:11
%%%-------------------------------------------------------------------
-module(elogic2_topicfs).
-author("zhengyinyong").

-include("elogic2.hrl").

-compile({parse_transform, lager_transform}).

%% API
-export([request_topicfs/1, generate_request_to_topicfs/3]).

request_topicfs(JsonBody) ->
    EtopicfsPostURL = elogic2_utils:get_env(elogic2, etopicfs, etopicfs_post_url),
    HttpTimeout = elogic2_utils:get_env(elogic2, etopicfs, http_timeout),
    case http_post_to_topicfs(EtopicfsPostURL, JsonBody, HttpTimeout) of
        {ok} ->
            {ok};
        Else ->
            Else
    end.

generate_request_to_topicfs(Type, Topic, Elogic2State) ->
    JsonArgs = case Type of
                   connect ->
                       generate_connect_request_to_topicfs(Elogic2State);
                   disconnect ->
                       generate_disconnect_request_to_topicfs(Elogic2State);
                   subscribe ->
                       generate_subscribe_request_to_topicfs(Topic, Elogic2State);
                   unsubscribe ->
                       generate_unsubscribe_request_to_topicfs(Topic, Elogic2State)
               end,
    jiffy:encode(JsonArgs).

%%%===================================================================
%%% Internal functions
%%%===================================================================
generate_connect_request_to_topicfs(Elogic2State) ->
    Uid = Elogic2State#elogic2_state.internal_package_args#internal_package_args.uid,
    ClientId = Elogic2State#elogic2_state.internal_package_args#internal_package_args.client_id,
    BrokerScore = Elogic2State#elogic2_state.internal_package_args#internal_package_args.broker_score,
    {[
        {<<"action">>, elogic2_utils:make_sure_binary(?TOPICFS_ACTION_CONNECT)},
        {<<"cid">>,    elogic2_utils:make_sure_binary(ClientId)},
        {<<"topic">>,  <<>>},
        {<<"uid">>,    elogic2_utils:make_sure_binary(Uid)},
        {<<"score">>,  elogic2_utils:make_sure_binary(BrokerScore)}
    ]}.

generate_disconnect_request_to_topicfs(Elogic2State) ->
    Uid = Elogic2State#elogic2_state.internal_package_args#internal_package_args.uid,
    ClientId = Elogic2State#elogic2_state.internal_package_args#internal_package_args.client_id,
    BrokerScore = Elogic2State#elogic2_state.internal_package_args#internal_package_args.broker_score,
    {[
        {<<"action">>, elogic2_utils:make_sure_binary(?TOPICFS_ACTION_DISCONNECT)},
        {<<"cid">>,    elogic2_utils:make_sure_binary(ClientId)},
        {<<"topic">>,  <<>>},
        {<<"uid">>,    elogic2_utils:make_sure_binary(Uid)},
        {<<"score">>,  elogic2_utils:make_sure_binary(BrokerScore)}
    ]}.

generate_subscribe_request_to_topicfs(Topic, Elogic2State) ->
    Uid = Elogic2State#elogic2_state.internal_package_args#internal_package_args.uid,
    ClientId = Elogic2State#elogic2_state.internal_package_args#internal_package_args.client_id,
    BrokerScore = Elogic2State#elogic2_state.internal_package_args#internal_package_args.broker_score,
    Appkey = Elogic2State#elogic2_state.internal_package_args#internal_package_args.appkey,
    Topic2 = filename:join(["/" ++ elogic2_utils:make_sure_string(Appkey) ++ "/" ++ elogic2_utils:make_sure_string(Topic)]),
    {[
        {<<"action">>, elogic2_utils:make_sure_binary(?TOPICFS_ACTION_SUBSCRIBE)},
        {<<"cid">>,    elogic2_utils:make_sure_binary(ClientId)},
        {<<"topic">>,  elogic2_utils:make_sure_binary(Topic2)},
        {<<"uid">>,    elogic2_utils:make_sure_binary(Uid)},
        {<<"score">>,  elogic2_utils:make_sure_binary(BrokerScore)}
    ]}.

generate_unsubscribe_request_to_topicfs(Topic, Elogic2State) ->
    Uid = Elogic2State#elogic2_state.internal_package_args#internal_package_args.uid,
    Appkey = Elogic2State#elogic2_state.internal_package_args#internal_package_args.appkey,
    ClientId = Elogic2State#elogic2_state.internal_package_args#internal_package_args.client_id,
    BrokerScore = Elogic2State#elogic2_state.internal_package_args#internal_package_args.broker_score,
    Topic2 = filename:join(["/" ++ elogic2_utils:make_sure_string(Appkey) ++ "/" ++ elogic2_utils:make_sure_string(Topic)]),
    {[
        {<<"action">>, elogic2_utils:make_sure_binary(?TOPICFS_ACTION_UNSUBSCRIBE)},
        {<<"cid">>,    elogic2_utils:make_sure_binary(ClientId)},
        {<<"topic">>,  elogic2_utils:make_sure_binary(Topic2)},
        {<<"uid">>,    elogic2_utils:make_sure_binary(Uid)},
        {<<"score">>,  elogic2_utils:make_sure_binary(BrokerScore)}
    ]}.

http_post_to_topicfs(RequestURL, Content, Timeout) ->
    case elogic2_utils:http_post(RequestURL, Content, Timeout) of
        {ok, _Body} ->
            {ok};
        Else ->
            Else
    end.
