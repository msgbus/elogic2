%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2016, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 04. 五月 2016 上午10:25
%%%-------------------------------------------------------------------
-module(elogic2_worker).
-author("zhengyinyong").

-behaviour(gen_server).

-include("elogic2.hrl").
-include_lib("yunba_mqtt_serialiser/include/yunba_mqtt_serialiser.hrl").
-include_lib("msgbus_common_utils/include/internal_package_pb.hrl").

-compile({parse_transform, lager_transform}).

%% API
-export([start_link/1]).

%% gen_server callbacks
-export([init/1,
    handle_call/3,
    handle_cast/2,
    handle_info/2,
    terminate/2,
    code_change/3]).

-define(SERVER, ?MODULE).

-record(state, {}).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @end
%%--------------------------------------------------------------------
start_link(Args) ->
    gen_server:start_link(?MODULE, Args, []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
-spec(init(Args :: term()) ->
    {ok, State :: #state{}} | {ok, State :: #state{}, timeout() | hibernate} |
    {stop, Reason :: term()} | ignore).
init(_Args) ->
    {ok, #state{}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
        State :: #state{}) ->
    {reply, Reply :: term(), NewState :: #state{}} |
    {reply, Reply :: term(), NewState :: #state{}, timeout() | hibernate} |
    {noreply, NewState :: #state{}} |
    {noreply, NewState :: #state{}, timeout() | hibernate} |
    {stop, Reason :: term(), Reply :: term(), NewState :: #state{}} |
    {stop, Reason :: term(), NewState :: #state{}}).
handle_call(_Request, _From, State) ->
    {reply, ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_cast(Request :: term(), State :: #state{}) ->
    {noreply, NewState :: #state{}} |
    {noreply, NewState :: #state{}, timeout() | hibernate} |
    {stop, Reason :: term(), NewState :: #state{}}).
handle_cast({package_from_mq, InternalPackage}, State) ->
    InternalPackageRecord = internal_package_pb:decode_internalpackage(InternalPackage),
    handle_internal_package(InternalPackageRecord),
    poolboy:checkin(?ELOGIC2_WORKER_POOL, self()),
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
-spec(handle_info(Info :: timeout() | term(), State :: #state{}) ->
    {noreply, NewState :: #state{}} |
    {noreply, NewState :: #state{}, timeout() | hibernate} |
    {stop, Reason :: term(), NewState :: #state{}}).
handle_info(_Info, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
        State :: #state{}) -> term()).
terminate(_Reason, _State) ->
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
-spec(code_change(OldVsn :: term() | {down, term()}, State :: #state{},
        Extra :: term()) ->
    {ok, NewState :: #state{}} | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
handle_internal_package(InternalPackageRecord) ->
    case generate_elogic2_state(InternalPackageRecord) of
        {ok, Elogic2State} ->
            MqttPackage = InternalPackageRecord#internalpackage.mqtt_package,
            ProtocolVersion = InternalPackageRecord#internalpackage.protocol_version,
            case yunba_mqtt_serialiser:parse(MqttPackage, ProtocolVersion) of
                {ok, MqttFrame, _} ->
                    handle_mqtt_package(MqttFrame, Elogic2State);
                {error, Reason} ->
                    lager:error("parse mqtt package error : ~p~n", [Reason])
            end;
        {error, Reason} ->
            lager:error("generate elogic2 state error : ~p~n", [Reason])
    end.

handle_mqtt_package(MqttFrame, Elogic2State) ->
    case MqttFrame#mqtt_frame.fixed#mqtt_frame_fixed.type of
        ?CONNECT ->
            handle_command_connect(MqttFrame, Elogic2State);
        ?DISCONNECT ->
            handle_command_disconnect(MqttFrame, Elogic2State);
        ?SUBSCRIBE ->
            handle_command_subscribe(MqttFrame, Elogic2State);
        ?UNSUBSCRIBE ->
            handle_command_unsubscribe(MqttFrame, Elogic2State);
        ?PUBLISH ->
            handle_command_publish(MqttFrame, Elogic2State);
        ?PUBREL ->
            handle_command_pubrel(MqttFrame, Elogic2State);
        ?PUBREC ->
            handle_command_pubrec(MqttFrame, Elogic2State);
        ?PUBCOMP ->
            handle_command_pubcomp(MqttFrame, Elogic2State);
        ?PUBACK ->
            handle_command_puback(MqttFrame, Elogic2State);
        ?PINGREQ ->
            handle_command_pingreq(MqttFrame, Elogic2State);
        Others ->
            lager:info("other package ~p~n", [Others])
    end.

handle_command_connect(MqttFrame, Elogic2State) ->
    elogic2_connect_handler:handle_connect(MqttFrame, Elogic2State).

handle_command_disconnect(MqttFrame, Elogic2State) ->
    elogic2_connect_handler:handle_disconnect(MqttFrame, Elogic2State).

handle_command_subscribe(MqttFrame, Elogic2State) ->
    elogic2_sub_unsub_handler:handle_subscribe(MqttFrame, Elogic2State).

handle_command_unsubscribe(MqttFrame, Elogic2State) ->
    elogic2_sub_unsub_handler:handle_unsubscribe(MqttFrame, Elogic2State).

handle_command_publish(MqttFrame, Elogic2State) ->
    elogic2_publish_handler:handle_publish(MqttFrame, Elogic2State).

handle_command_pubrel(MqttFrame, Elogic2State) ->
    elogic2_publish_handler:handle_pubrel(MqttFrame, Elogic2State).

handle_command_pubrec(MqttFrame, Elogic2State) ->
    elogic2_publish_handler:handle_pubrec(MqttFrame, Elogic2State).

handle_command_pubcomp(MqttFrame, Elogic2State) ->
    elogic2_publish_handler:handle_pubcomp(MqttFrame, Elogic2State).

handle_command_puback(MqttFrame, Elogic2State) ->
    elogic2_publish_handler:handle_puback(MqttFrame, Elogic2State).

handle_command_pingreq(MqttFrame, Elogic2State) ->
    elogic2_connect_handler:handle_connect(MqttFrame, Elogic2State).

generate_elogic2_state(InternalPackageRecord) ->
    %% get args from Internal Package
    BrokerScore = InternalPackageRecord#internalpackage.from_tag,
    ProtocolVersion = InternalPackageRecord#internalpackage.protocol_version,
    Uid = InternalPackageRecord#internalpackage.from_uid,
    ClientId = InternalPackageRecord#internalpackage.client_id,
    Appkey = InternalPackageRecord#internalpackage.appkey,
    Platform = InternalPackageRecord#internalpackage.platform,
    FromIp = InternalPackageRecord#internalpackage.from_ip,
    FromPort = InternalPackageRecord#internalpackage.from_port,

    case elogic2_utils:clientid_to_appid(ClientId) of
        {ok, AppId} ->
            case elogic2_utils:verify_appkey(Appkey) of
                ok ->
                    Elogic2State = #elogic2_state{
                        internal_package_args = #internal_package_args {
                            broker_score = BrokerScore,
                            protocol_version = ProtocolVersion,
                            uid = Uid,
                            client_id = ClientId,
                            appkey = Appkey,
                            appid = AppId,
                            platform = Platform,
                            from_ip = FromIp,
                            from_port = FromPort
                        }},
                        {ok, Elogic2State};
                {error, Reason1} ->
                    {error, Reason1}
            end;
        {error, Reason2} ->
            {error, Reason2}
    end.