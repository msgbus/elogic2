%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2016, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 06. 五月 2016 上午11:49
%%%-------------------------------------------------------------------
-module(elogic2_sub_unsub_handler).
-author("zhengyinyong").

-include("elogic2.hrl").
-include_lib("yunba_mqtt_serialiser/include/yunba_mqtt_serialiser.hrl").
-include_lib("msgbus_common_utils/include/internal_package_pb.hrl").

-compile({parse_transform, lager_transform}).

%% API
-export([handle_subscribe/2, handle_unsubscribe/2]).

handle_subscribe(MqttFrame, Elogic2State) ->
    do_sub_unsub(subscribe, MqttFrame, Elogic2State).

handle_unsubscribe(MqttFrame, Elogic2State) ->
    do_sub_unsub(unsubscribe, MqttFrame, Elogic2State).

%%%===================================================================
%%% Internal functions
%%%===================================================================
do_sub_unsub(Type, MqttFrame, Elogic2State) ->
    TopicTable = MqttFrame#mqtt_frame.variable#mqtt_frame_subscribe.topic_table,
    Result = lists:map(
        fun(Topic) ->
            elogic2_topicfs:request_topicfs(elogic2_topicfs:generate_request_to_topicfs(Type, Topic#mqtt_topic.name, Elogic2State))
        end, TopicTable),

    IsAllOK = lists:all(fun(R) ->
                            case R of
                                {ok} ->
                                    true;
                                _ ->
                                    false
                            end
                        end, Result),

    SendAck = case IsAllOK of
                  true ->
                      send_ack(Type, MqttFrame, Elogic2State);
                  false ->
                      ignore
              end,
    SendAck.

send_ack(Type, MqttFrame, Elogic2State) ->
    InternalPackage = case Type of
                           subscribe ->
                               elogic2_package_formatter:form_internal_package_suback(MqttFrame, Elogic2State);
                           unsubscribe ->
                               elogic2_package_formatter:form_internal_package_unsuback(MqttFrame, Elogic2State)
                       end,
    FrontTag = Elogic2State#elogic2_state.internal_package_args#internal_package_args.broker_score,
    elogic2_publish_handler:send_to_front(FrontTag, InternalPackage).
