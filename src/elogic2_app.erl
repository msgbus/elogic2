-module(elogic2_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
    set_ibrowse_max_sessions_for_etopicfs(),
    elogic2_sup:start_link().

stop(_State) ->
    ok.

%%%===================================================================
%%% Internal functions
%%%===================================================================
set_ibrowse_max_sessions_for_etopicfs() ->
    ArgsForGet = elogic2_utils:get_env(elogic2, etopicfs, etopicfs_ibrowse_settings_for_get),
    ArgsForPost = elogic2_utils:get_env(elogic2, etopicfs, etopicfs_ibrowse_settings_for_post),
    ibrowse:set_max_sessions(proplists:get_value(host, ArgsForGet),
        proplists:get_value(port, ArgsForGet),
        proplists:get_value(max_sessions, ArgsForGet)),
    ibrowse:set_max_sessions(proplists:get_value(host, ArgsForPost),
        proplists:get_value(port, ArgsForPost),
        proplists:get_value(max_sessions, ArgsForPost)).
